import { Component } from "@angular/core";
import { Property, SettingsService, Store } from "@csiro-geoanalytics/ng";

import * as _ from "lodash";

const sleep = (ms: number) => new Promise(resolve => _.delay(resolve, ms));

@Component({
	selector: "app-root",
	templateUrl: "./app.component.html",
	styleUrls: [ "./app.component.css" ]
})
export class AppComponent
{
	constructor(
		private settings: SettingsService
	)
	{
		// Test SettingService's session storage.
		const prop: Property = { key: "session_test", store: Store.both };
		this.settings
			.set(prop, "ONE")
			.then(() => this.settings.get(prop, "def"))
			.then(val => val === "ONE")
			.then(val => console.log("sessionStorage: ", val))
		;

		// Test SettingService's expirable storage.
		this.settings
			.getExpirable("dontCache", 9)
				.then(val => val === 9)
				.then(val => console.log("setExpirable/dontCache: ", val))
			.then(() => this.settings.setExpirable("test", 1, { expiry: 5 }))
			.then(() => this.settings.setExpirable("test", 2, { expiry: 2 }))
				.then(val => val === 2)
				.then(val => console.log("setExpirable/setOverride: ", val))
			.then(() => sleep(500))
			.then(() => this.settings.getExpirable("test", 0))
				.then(val => val === 2)
				.then(val => console.log("getExpirable/cached: ", val))
			.then(() => sleep(2100))
			.then(() => this.settings.getExpirable("test", 0))
				.then(val => val === 0)
				.then(val => console.log("getExpirable/expired: ", val))
		;
	}
}
