import { TestBed, waitForAsync } from "@angular/core/testing";
import { AppComponent } from "./app.component";
import { GoogleAnalyticsModule } from "../../projects/ng-lib/src/public_api";


describe("AppComponent", () =>
{
	beforeEach(waitForAsync(() =>
	{
		TestBed.configureTestingModule({
			imports: [
				GoogleAnalyticsModule.forRoot({
					key			: "TEST",
					plugins		: [ "eventTracker", "outboundLinkTracker", "urlChangeTracker" ]
				})
			],
			declarations: [
				AppComponent
			]
		}).compileComponents();
	}));

	it("should create the app", waitForAsync(() =>
	{
		const fixture = TestBed.createComponent(AppComponent);
		const app = fixture.debugElement.componentInstance;
		expect(app).toBeTruthy();
	}));

	it("should render title in a h1 tag", waitForAsync(() =>
	{
		const fixture = TestBed.createComponent(AppComponent);
		fixture.detectChanges();
		const compiled = fixture.debugElement.nativeElement;
		expect(compiled.querySelector("h1").textContent).toContain("CSIRO Geoanalytics Group");
	}));

	it("should render subtitle in a h2 tag", waitForAsync(() =>
	{
		const fixture = TestBed.createComponent(AppComponent);
		fixture.detectChanges();
		const compiled = fixture.debugElement.nativeElement;
		expect(compiled.querySelector("h2").textContent).toContain("Angular services and components library");
	}));
});
