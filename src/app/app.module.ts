import { BrowserModule } from "@angular/platform-browser";
import { NgModule } from "@angular/core";
import { LocalStorageModule, LOCALSTORAGE_CONFIG, SettingsModule } from "@csiro-geoanalytics/ng";

// App root.
import { AppComponent } from "./app.component";


@NgModule({
	imports: [
		BrowserModule,
		LocalStorageModule,
		SettingsModule.forRoot()
	],
	declarations	: [ AppComponent ],
	bootstrap		: [ AppComponent ],
	providers: [
		{ provide: LOCALSTORAGE_CONFIG, useValue: { name: "STORE", storeName: "default" } }
	]
})
export class AppModule {}
