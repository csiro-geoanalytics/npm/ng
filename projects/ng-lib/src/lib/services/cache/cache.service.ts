import { Injectable } from "@angular/core";
import { LocalStorageService } from "../local-storage";
import { SettingsService, Property, Store } from "../settings";
import { LocalSorageCache } from "./local-storage-cache";
import { Readiable } from "../../utils/readiable";

import * as _ from "lodash";
import * as TT from "./cache.service.types";
import * as DEF from "./cache.service.defaults";


@Injectable()
export class CacheService extends Readiable
{
	private config					: TT.CacheSettings = DEF.DEFAULT_SETTINGS;
	private cacheSettings			: Property = { store: Store.local, key: "cache" };


	constructor(
		private settings			: SettingsService,
		private localStorage		: LocalStorageService
	)
	{
		super();

		// App settings ready/reload events.
		this.settings.ready
			.then(() => this.loadSettings())
			.finally(this.readyResolver)
		;
		this.settings.reload$.subscribe(() => this.loadSettings());
		this.settings.userChanged$.subscribe(() => this.dropCacheStores());
	}

	/*
	########  ########   #######  ########  ######## ########  ######## #### ########  ######
	##     ## ##     ## ##     ## ##     ## ##       ##     ##    ##     ##  ##       ##    ##
	##     ## ##     ## ##     ## ##     ## ##       ##     ##    ##     ##  ##       ##
	########  ########  ##     ## ########  ######   ########     ##     ##  ######    ######
	##        ##   ##   ##     ## ##        ##       ##   ##      ##     ##  ##             ##
	##        ##    ##  ##     ## ##        ##       ##    ##     ##     ##  ##       ##    ##
	##        ##     ##  #######  ##        ######## ##     ##    ##    #### ########  ######
	*/

	get enabled()
	{
		return this.config.enabled;
	}

	set enabled(value: boolean)
	{
		if (!(this.config.enabled = value))
			this.clearCacheStores();
		this.settings.set(this.cacheSettings, this.config);
	}

	get expiry()
	{
		return this.config.expiry;
	}

	set expiry(value: number)
	{
		this.config.expiry = value;
		this.settings.set(this.cacheSettings, this.config);
	}

	get usage(): Promise<TT.CacheUsageStats>
	{
		const ret = <TT.CacheUsageStats>{
			totalStores		: 0,
			totalRecords	: 0
		};

		return this.localStorage.getNonSystemStoreNames()
			.then(stores => {
				ret.totalStores = stores.length;
				return Promise.all(_.map(stores, store => this.localStorage.getInstance(store).length()));
			})
			.then(results => {
				ret.totalRecords = _.sum(results);
				return ret;
			});
	}

	/*
	##     ## ######## ######## ##     ##  #######  ########   ######
	###   ### ##          ##    ##     ## ##     ## ##     ## ##    ##
	#### #### ##          ##    ##     ## ##     ## ##     ## ##
	## ### ## ######      ##    ######### ##     ## ##     ##  ######
	##     ## ##          ##    ##     ## ##     ## ##     ##       ##
	##     ## ##          ##    ##     ## ##     ## ##     ## ##    ##
	##     ## ########    ##    ##     ##  #######  ########   ######
	*/

	private async loadSettings()
	{
		return this.settings.get(this.cacheSettings, DEF.DEFAULT_SETTINGS)
			.then(config => this.config = _.defaults(config, DEF.DEFAULT_SETTINGS));
	}

	getCacheInstance(project: string): LocalSorageCache
	{
		if (!project)
			return null;

		if (project.startsWith("//"))
			throw new Error(`Use of "${ project }" is prohibited.`);

		return new LocalSorageCache(
			this.localStorage.getInstance(project),
			{
				enabled: () => this.enabled,
				expiry: () => this.expiry
			}
		);
	}

	async clearCacheStores()
	{
		return this.localStorage.getNonSystemStoreNames()
			.then(names => names.reduce(async (prev, next) => {
					await prev;
					return this.localStorage.getInstance(next).clear();
				}, Promise.resolve())
			);
	}

	async dropCacheStores()
	{
		return this.localStorage.getNonSystemStoreNames()
			.then(names => this.localStorage.dropInstances(names));
	}
}
