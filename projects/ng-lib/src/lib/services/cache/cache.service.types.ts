export interface CacheSettings
{
	enabled			: boolean;
	expiry			: number;
}

export interface CacheUsageStats
{
	totalStores		: number;
	totalRecords	: number;
}

export interface ExpirableRecord<T>
{
	expires			: Date;
	value			: T;
}

export interface LocalStorageCacheConfig
{
	enabled			: () => boolean;
	expiry			: () => number;
}

export interface LocalStorageCacheOptions<T>
{
	expiry			: number | string | Date;
	cacheCondition	: (data: T) => boolean;
}
