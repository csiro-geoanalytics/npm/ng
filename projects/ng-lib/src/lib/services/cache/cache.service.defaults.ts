import * as TT from "./cache.service.types";

export const DEFAULT_SETTINGS: TT.CacheSettings = {
	enabled			: true,
	expiry			: 86400 // 24 Hrs
};
