import { Observable, of, from } from "rxjs";
import { map, switchMap } from "rxjs/operators";

import * as _		from "lodash";
import * as TT		from "./cache.service.types";
import * as DEF		from "./cache.service.defaults";


export class LocalSorageCache
{
	private storage				: LocalForage;
	private options				: Partial<TT.LocalStorageCacheConfig>;


	constructor(storage: LocalForage, options?: Partial<TT.LocalStorageCacheConfig>)
	{
		this.storage = storage;
		this.options = options;
	}

	/**
	 * Cache or use result from observable.
	 *
	 * If cache key does not exist or is expired, observable supplied in argument is returned and result cached.
	 *
	 * @param key Cache key.
	 * @param observable Source observable.
	 * @param options Cache options.
	 * @returns Observable of a cached value.
	 */
	observable<T>(key: string, observable: Observable<T>, options?: Partial<TT.LocalStorageCacheOptions<T>>): Observable<T>
	{
		return (
			((this.options?.enabled && this.options?.enabled()) ?? true) && this.storage
				// First fetch the item from localStorage (even though it may not exist).
				? from(this.storage.getItem(key))
					.pipe(
						// If the cached value has expired, nullify it, otherwise pass it through.
						map((val: TT.ExpirableRecord<T>) => val && new Date(val.expires).getTime() > Date.now() ? val : null),

						// At this point, if we encounter a null value, either it doesn't exist in the cache or it has expired.
						// If it doesn't exist, simply return the observable that has been passed in, caching its value as it passes through.
						switchMap(val => val
							? of(val.value)
							: observable.pipe(
								switchMap(cachableValue => options?.cacheCondition && options.cacheCondition(cachableValue) === false
									? of(cachableValue)
									: this.cache(key, cachableValue, options?.expiry)
								)
							)
						)
					)
				: observable // Skip caching, if it's disabled.
		);
	}

	/**
	 * Cache supplied value until expiry.
	 *
	 * @param key Cache key.
	 * @param value Value to cache.
	 * @param expiry Expiry date (e.g., specific dates or seconds from now).
	 * @returns An observable of a cached value.
	 */
	private cache<T>(key: string, value: T, expiry?: number | string | Date): Observable<T>
	{
		const expiryDate = expiry ?? (this.options?.expiry() ?? DEF.DEFAULT_SETTINGS.expiry);
		return this.storage
			?
				from(
					this.storage.setItem(key, {
						expires: this.sanitizeAndGenerateDateExpiry(expiryDate),
						value
					})
				)
				.pipe(
					map(val => val.value)
				)
			: of(value)
		;
	}

	/**
	 * Invalidates a specific key in the cache.
	 */
	invalidate(key: string): Promise<void>
	{
		if (!this.storage)
			return Promise.resolve();
		return this.storage.removeItem(key);
	}

	/**
	 * Clear current storage.
	 */
	clear(): Promise<void>
	{
		if (!this.storage)
			return Promise.resolve();
		return this.storage.clear();
	}

	/*
	##     ## ######## ##       ########  ######## ########   ######
	##     ## ##       ##       ##     ## ##       ##     ## ##    ##
	##     ## ##       ##       ##     ## ##       ##     ## ##
	######### ######   ##       ########  ######   ########   ######
	##     ## ##       ##       ##        ##       ##   ##         ##
	##     ## ##       ##       ##        ##       ##    ##  ##    ##
	##     ## ######## ######## ##        ######## ##     ##  ######
	*/

	/**
	 * Checks and formats the expiry date.
	 */
	private sanitizeAndGenerateDateExpiry(expiry: string | number | Date): Date
	{
		const expiryDate = this.expiryToDate(expiry);

		// Don't allow expiry dates in the past.
		if (expiryDate.getTime() <= Date.now())
			return new Date(Date.now() + (this.options?.expiry() ?? DEF.DEFAULT_SETTINGS.expiry));

		return expiryDate;
	}

	/**
	 * Generates an expiry data for various input types.
	 */
	private expiryToDate(expiry: number | string | Date): Date
	{
		if (_.isNumber(expiry))
			return new Date(Date.now() + Math.abs(expiry) * 1000);
		if (_.isString(expiry))
			return new Date(expiry);
		if (_.isDate(expiry))
			return expiry;
		return new Date();
	}
}
