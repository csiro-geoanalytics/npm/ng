import { Injectable, PLATFORM_ID, inject } from "@angular/core";
import { isPlatformBrowser } from "@angular/common";
import { Readiable } from "../../utils/readiable";

import * as _ from "lodash";


export class GenericSdkConfig
{
	key								: string;
	autoInject?						: boolean;
	headlessBrowsers?				: boolean;
}

const DEFAULT_CONFIG: Partial<GenericSdkConfig> = {

	autoInject						: true,
	headlessBrowsers				: true

};

@Injectable()
export abstract class GenericSdk extends Readiable
{
	private platformId				= inject(PLATFORM_ID);

	protected injected				= false;

	constructor(
		readonly config				: GenericSdkConfig
	)
	{
		super();

		// Can be initialised later upon SDK injection.
		if (!this.config)
			return;

		// Set default properties.
		_.defaultsDeep(this.config, DEFAULT_CONFIG);

		// Don't inject SDK for prerender.io and crawlers.
		if (this.config.key && this.config.autoInject && this.allowInjectionForThisBrowser())
			this.inject();
	}

	protected assertApiKey(config: GenericSdkConfig = this.config)
	{
		if (!config.key)
			throw new Error("API key is not set.");
		return true;
	}

	protected allowInjectionForThisBrowser(config: GenericSdkConfig = this.config)
	{
		if (!isPlatformBrowser(this.platformId))
			return false;
		return config.headlessBrowsers || !config.headlessBrowsers && !/Headless/gi.test(window.navigator.userAgent);
	}

	abstract inject(config?: GenericSdkConfig): Promise<any>;
}
