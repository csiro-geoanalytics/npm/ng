import { NgModule, ModuleWithProviders, Optional, SkipSelf } from "@angular/core";
import { GoogleMapsService, GoogleMapsServiceConfig } from "./maps.service";


@NgModule()
export class GoogleMapsModule
{
	constructor(@Optional() @SkipSelf() parentModule: GoogleMapsModule)
	{
		if (parentModule)
			throw new Error("GoogleMapsModule is already loaded.");
	}

	static forRoot(config?: GoogleMapsServiceConfig): ModuleWithProviders<GoogleMapsModule>
	{
		return {
			ngModule: GoogleMapsModule,
			providers: [
				{ provide: GoogleMapsServiceConfig, useValue: config },
				{ provide: GoogleMapsService }
			]
		};
	}
}
