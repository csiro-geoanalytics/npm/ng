import { Injectable } from "@angular/core";
import { GenericSdk, GenericSdkConfig } from "../../generic-sdk.service";

import * as _ from "lodash";

export class RecaptchaServiceConfig extends GenericSdkConfig {}

@Injectable()
export class RecaptchaService extends GenericSdk
{
	constructor(
		readonly config				: RecaptchaServiceConfig
	)
	{
		super(config);
	}

	inject(config?: RecaptchaServiceConfig)
	{
		if (this.injected)
			return this.ready;

		const cfg = { ...this.config, ...config };

		this.assertApiKey(cfg);
		if (this.allowInjectionForThisBrowser(cfg))
		{
			((d, tag, path) => {
				const el: any = d.createElement(tag),
				first = d.getElementsByTagName(tag)[0];
				el.async = true;
				el.defer = true;
				el.onload = this.readyResolver;
				el.onerror = this.readyRejector;
				el.src = path;
				first.parentNode.insertBefore(el, first);
			})(document, "script", "//www.google.com/recaptcha/api.js?render=explicit");

			// Update config.
			_.assign(this.config, cfg);
			this.injected = true;
		}
		else
			this.readyRejector();

		return this.ready;
	}
}
