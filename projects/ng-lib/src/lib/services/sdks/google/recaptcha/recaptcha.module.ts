import { NgModule, ModuleWithProviders } from "@angular/core";
import { CommonModule } from "@angular/common";
import { RecaptchaService, RecaptchaServiceConfig } from "./recaptcha.service";
import { RecaptchaComponent } from "./recaptcha.component";


@NgModule({
	imports: [ CommonModule ],
	declarations: [ RecaptchaComponent ],
	exports: [ RecaptchaComponent ]
})
export class RecaptchaModule
{
	/*
		Import RecaptchaModule into the app module using forRoot method that will inject a global preconfigured RecaptchaService that
		ensures that Google reCAPTCHA API script is injected once only:

			@NgModule({
				imports: [
					...
					RecaptchaModule.forRoot({ key: "YOUR_API_KEY" })
				]
			})
			export class AppModule
			{
			}

		Then import RecaptchaModule into each feature module (including, lazy-loaded modules) as normally. This will enable the use of the
		RecaptchaComponent in component templates:

			@NgModule({
				imports: [
					...
					RecaptchaModule
				]
			})
			export class FeatureModule
			{
			}
	*/
	static forRoot(config?: RecaptchaServiceConfig): ModuleWithProviders<RecaptchaModule>
	{
		return {
			ngModule: RecaptchaModule,
			providers: [
				{ provide: RecaptchaServiceConfig, useValue: config },
				{ provide: RecaptchaService }
			]
		};
	}
}
