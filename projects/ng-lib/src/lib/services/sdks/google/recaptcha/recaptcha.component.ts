import { Component, ViewChild, ElementRef, Input, Self, Optional, OnDestroy } from "@angular/core";
import { ControlValueAccessor, NgControl } from "@angular/forms";
import { GenericRetryOperation } from "@csiro-geoanalytics/utils-rxjs";
import { RecaptchaService } from "./recaptcha.service";

declare let jQuery: any;
declare let grecaptcha: any;


export type RecaptchaTheme = "light" | "dark";
export type RecaptchaSize = "normal" | "compact";

@Component({
	// eslint-disable-next-line @angular-eslint/component-selector
	selector: "recaptcha",
	template: "<div #recaptcha></div>"
})
export class RecaptchaComponent implements ControlValueAccessor, OnDestroy
{
	@Input() theme				: RecaptchaTheme = "light";
	@Input() size				: RecaptchaSize = "normal";

	@ViewChild("recaptcha", { static: true })
	element						: ElementRef;

	private api					: RecaptchaAPI;
	private widget				: any;
	private onChange			: (val: string) => void;
	private onTouched			: () => void;

	constructor(
		@Self()
		@Optional()
		private ngControl		: NgControl,
		private recaptcha		: RecaptchaService
	)
	{
		// Setting the value accessor directly (instead of using the providers) to avoid running into a circular import.
		if (this.ngControl != null)
			this.ngControl.valueAccessor = this;

		// Wait for the RecaptchaService to initialise.
		this.recaptcha.ready
			.then(() => this.renderWidget())
			.catch(() => this.handleError("Recaptcha failed to load."));
	}

	ngOnDestroy()
	{
		// Clean-up Recaptcha elements.
		if (jQuery)
			jQuery("DIV:has(DIV.g-recaptcha-bubble-arrow)").remove();
	}

	/**
	 * Recaptcha API loads a loader script that in turn loads another script for a particular version of the API.
	 * The following retryable waits until that other script is loaded as there's no obvious way to bind to its onload event.
	 */
	private renderWidget()
	{
		GenericRetryOperation(
			() => !!grecaptcha.render,
			{
				next: () => {
					// Initialise Recaptcha widget.
					this.api = grecaptcha;
					this.widget = this.api.render(this.element.nativeElement, {
						"sitekey"			: this.recaptcha.config.key,
						"theme"				: this.theme,
						"size"				: this.size,
						"callback"			: (token: string) => this.recaptchaValidated(token),
						"expired-callback"	: () => this.reset(),
						"error-callback"	: () => this.reset()
					});
				}
			},
			{
				maxRetryAttempts	: 10,
				retryInterval		: 200
			}
		);
	}

	private handleError(...args: any)
	{
		console.error(...args);
		this.reset();
	}

	reset(): void
	{
		if (this.api)
			this.api.reset(this.widget);
		if (this.onChange)
			this.onChange(null);
	}

	get value(): string
	{
		const token = this.api.getResponse(this.widget);
		return !token ? null : token;
	}

	private recaptchaValidated(token: string)
	{
		this.onChange(token);
		this.onTouched();
	}

	/*
	  #####                                           #     #                                #
	 #     #  ####  #    # ##### #####   ####  #      #     #   ##   #      #    # ######   # #    ####   ####  ######  ####   ####   ####  #####
	 #       #    # ##   #   #   #    # #    # #      #     #  #  #  #      #    # #       #   #  #    # #    # #      #      #      #    # #    #
	 #       #    # # #  #   #   #    # #    # #      #     # #    # #      #    # #####  #     # #      #      #####   ####   ####  #    # #    #
	 #       #    # #  # #   #   #####  #    # #       #   #  ###### #      #    # #      ####### #      #      #           #      # #    # #####
	 #     # #    # #   ##   #   #   #  #    # #        # #   #    # #      #    # #      #     # #    # #    # #      #    # #    # #    # #   #
	  #####   ####  #    #   #   #    #  ####  ######    #    #    # ######  ####  ###### #     #  ####   ####  ######  ####   ####   ####  #    #
	*/

	writeValue(val: any)
	{
		this.reset();
	}

	registerOnChange(fn: any)
	{
		this.onChange = fn;
	}

	registerOnTouched(fn: any)
	{
		this.onTouched = fn;
	}
}

interface RecaptchaAPI
{
	render			: (elementId: string, opts: any) => number;
	getResponse		: (widgetId: number) => any;
	reset			: (widgetId: number) => void;
}
