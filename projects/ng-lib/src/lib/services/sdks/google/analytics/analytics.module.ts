import { NgModule, ModuleWithProviders, Optional, SkipSelf } from "@angular/core";
import { GoogleAnalyticsService, GoogleAnalyticsServiceConfig } from "./analytics.service";


@NgModule()
export class GoogleAnalyticsModule
{
	constructor(@Optional() @SkipSelf() parentModule: GoogleAnalyticsModule)
	{
		if (parentModule)
			throw new Error("GoogleAnalyticsModule is already loaded.");
	}

	static forRoot(config?: GoogleAnalyticsServiceConfig): ModuleWithProviders<GoogleAnalyticsModule>
	{
		return {
			ngModule: GoogleAnalyticsModule,
			providers: [
				{ provide: GoogleAnalyticsServiceConfig, useValue: config },
				{ provide: GoogleAnalyticsService }
			]
		};
	}
}
