import { Injectable } from "@angular/core";
import { BehaviorSubject } from "rxjs";
import { GenericSdk, GenericSdkConfig } from "../../generic-sdk.service";

import * as _ from "lodash";

declare let ga: (...params: any) => void;


export class GoogleAnalyticsServiceConfig extends GenericSdkConfig
{
	plugins?: string[];
}

@Injectable()
export class GoogleAnalyticsService extends GenericSdk
{
	private _enabled				: boolean;

	// Events.
	private eventEnabled			= new BehaviorSubject<boolean>(false);
	readonly enabled$				= this.eventEnabled.asObservable();

	private adBlockerDetected		= new BehaviorSubject<boolean>(false);
	readonly adBlockerDetected$		= this.adBlockerDetected.asObservable();


	constructor(
		readonly config: GoogleAnalyticsServiceConfig
	)
	{
		super(config);
	}

	inject(config?: GoogleAnalyticsServiceConfig)
	{
		if (this.injected)
			return this.ready;

		const cfg = { ...this.config, ...config };

		this.assertApiKey(cfg);
		if (this.allowInjectionForThisBrowser(cfg))
		{
			// Inject Google Analytics script.
			((i, s, o, g, r, a, m) => {
				// eslint-disable-next-line @typescript-eslint/dot-notation
				i["GoogleAnalyticsObject"] = r;
				i[r] = i[r] || (() => { (i[r].q = i[r].q || []).push(arguments); });
				i[r].l = +new Date();
				a = s.createElement(o);
				m = s.getElementsByTagName(o)[0];
				a.async = true;
				a.defer = true;
				a.onload = this.readyResolver;
				a.onerror = this.readyRejector;
				a.src = g;
				m.parentNode.insertBefore(a, m);
			})(window, document, "script", "//www.google-analytics.com/analytics.js", "ga");

			// Update config.
			_.assign(this.config, cfg);
			this.injected = true;

			this.ready
				// Configure tracker.
				.then(() => {
					ga("create", cfg.key, "auto");
					_.each(cfg.plugins, plugin => ga("require", plugin));
					ga("send", "pageview");
				})
				// Enable the service initially.
				.then(() => this.setEnabled(true))
				.then(() => this.detectAdBlocker())
			;
		}
		else
			this.deactivate();

		return this.ready;
	}

	private deactivate()
	{
		this.setEnabled(false);
		this.readyRejector();
	}

	get enabled()
	{
		return this._enabled;
	}

	setEnabled(value: boolean)
	{
		this.assertApiKey();
		window["ga-disable-" + this.config.key] = !(this._enabled = value);
		this.eventEnabled.next(value);
	}

	private detectAdBlocker()
	{
		this.assertApiKey();

		// The magic sequence might potentially change if analytics.js script is updated.
		this.adBlockerDetected.next(ga instanceof Function && ga.toString().replace(/[^\w]/g, "") !== "functionaJ1ZDapplyZarguments");
	}

	emitEvent(category: string, action: string, label: string, value?: number)
	{
		if (this.enabled)
		{
			ga("send", "event", {
				eventCategory: category,
				eventAction: action,
				eventLabel: label,
				eventValue: value
			});
		}
	}

	/*
		User timings.
		https://developers.google.com/analytics/devguides/collection/analyticsjs/user-timings
	*/

	startTiming(category: string, variable: string, label?: string): TimingHandle
	{
		const start = _.now();
		return <TimingHandle>{
			end: () => this.emitTiming(<TimingDescriptor>{ category, variable, label, start })
		};
	}

	private emitTiming(handle: TimingDescriptor): number
	{
		if (!this.enabled)
			return null;
		const time = _.now() - handle.start;
		ga("send", "timing", handle.category, handle.variable, time, handle.label);
		return time;
	}
}

/*
 #     #                         #######
 #     #  ####  ###### #####        #    # #    # # #    #  ####   ####
 #     # #      #      #    #       #    # ##  ## # ##   # #    # #
 #     #  ####  #####  #    #       #    # # ## # # # #  # #       ####
 #     #      # #      #####        #    # #    # # #  # # #  ###      #
 #     # #    # #      #   #        #    # #    # # #   ## #    # #    #
  #####   ####  ###### #    #       #    # #    # # #    #  ####   ####
*/

export interface TimingDescriptor
{
	category:	string;
	variable:	string;
	label?:		string;
	start:		number;
}

export interface TimingHandle
{
	end: () => number;
}
