import { NgModule, ModuleWithProviders, Optional, SkipSelf } from "@angular/core";
import { YoutubeService, YoutubeServiceConfig } from "./youtube.service";


@NgModule()
export class YoutubeModule
{
	constructor(@Optional() @SkipSelf() parentModule: YoutubeModule)
	{
		if (parentModule)
			throw new Error("YoutubeModule is already loaded.");
	}

	static forRoot(config?: YoutubeServiceConfig): ModuleWithProviders<YoutubeModule>
	{
		return {
			ngModule: YoutubeModule,
			providers: [
				{ provide: YoutubeServiceConfig, useValue: config },
				{ provide: YoutubeService }
			]
		};
	}
}
