export interface YtResultItem
{
	videoId: string;
	title: string;
	description: string;
	publishedAt: string;
	thumbnails: YtThumbnails;
	imageUrl?: string;
}

export interface YtThumbnails
{
	default: YtThumbnail;
	medium: YtThumbnail;
	high: YtThumbnail;
}

export interface YtThumbnail
{
	url: string;
	width: number;
	height: number;
}
