import { Injectable } from "@angular/core";
import { GenericSdk, GenericSdkConfig } from "../../generic-sdk.service";

import * as _ from "lodash";

export class YoutubeServiceConfig extends GenericSdkConfig {}


@Injectable()
export class YoutubeService extends GenericSdk
{
	constructor(
		readonly config				: YoutubeServiceConfig
	)
	{
		super(config);
	}

	inject(config?: YoutubeServiceConfig)
	{
		if (this.injected)
			return this.ready;

		const cfg = { ...this.config, ...config };

		this.assertApiKey();
		if (this.allowInjectionForThisBrowser(cfg))
		{
			((d, tag, path) => {
				const el: any = d.createElement(tag),
				first = d.getElementsByTagName(tag)[0];
				el.async = true;
				el.defer = true;
				el.onload = this.readyResolver;
				el.onerror = this.readyRejector;
				el.src = path;
				first.parentNode.insertBefore(el, first);
			})(document, "script", "https://www.youtube.com/iframe_api");

			// Update config.
			_.assign(this.config, cfg);
			this.injected = true;
		}
		else
			this.readyRejector();

		return this.ready;
	}
}
