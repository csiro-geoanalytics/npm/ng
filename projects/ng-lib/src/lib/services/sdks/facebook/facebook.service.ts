import { Injectable } from "@angular/core";
import { Meta } from "@angular/platform-browser";
import { GenericSdk, GenericSdkConfig } from "../generic-sdk.service";

import * as _ from "lodash";

declare const FB: any;


export class FacebookServiceConfig extends GenericSdkConfig
{
	pageId?							: string;
	greetingDialogDisplay?			: string;	// default: "hide"
	themeColor?						: string;
	admins?							: string;
	xfbml?							: boolean;	// default: true
	version?						: string;	// default: "v10.0"
}

const DEFAULT_CONFIG: Partial<FacebookServiceConfig> = {

	greetingDialogDisplay			: "hide",
	xfbml							: true,
	version							: "v10.0"

};

@Injectable()
export class FacebookService extends GenericSdk
{
	constructor(
		readonly config				: FacebookServiceConfig,
		private meta				: Meta
	)
	{
		super(config);

		this.meta.addTag({ property: "fb:pages", content: config.pageId });
		if (config.admins)
			this.meta.addTag({ property: "fb:admins", content: config.admins });
	}

	inject(config?: FacebookServiceConfig)
	{
		if (this.injected)
			return this.ready;

		const cfg = { ...DEFAULT_CONFIG, ...this.config, ...config };
		if (!cfg.pageId)
			cfg.pageId = cfg.key;

		this.assertApiKey(cfg);

		if (this.allowInjectionForThisBrowser(cfg))
		{
			// Async initialisation of the Facebook SDK.
			(<any>window).fbAsyncInit = () => {
				FB.init({
					xfbml	: cfg.xfbml,
					version	: cfg.version
				});
			};

			// Add customer chat element.
			const div = document.createElement("div");
			div.className = "fb-customerchat";
			div.setAttribute("attribution", "setup_tool");
			div.setAttribute("greeting_dialog_display", cfg.greetingDialogDisplay);
			div.setAttribute("page_id", cfg.pageId);
			if (cfg.themeColor)
				div.setAttribute("theme_color", cfg.themeColor);
			document.body.appendChild(div);

			// Inject SDK script.
			((d, s, id) => {
				const fjs = d.getElementsByTagName(s)[0];
				if (d.getElementById(id))
					return;
				const js: any = d.createElement(s);
				js.id = id;
				js.src = "https://connect.facebook.net/en_US/sdk/xfbml.customerchat.js";
				js.async = true;
				js.defer = true;
				js.crossorigin = "anonymous";
				js.onload = this.readyResolver;
				js.onerror = this.readyRejector;
				fjs.parentNode.insertBefore(js, fjs);
			})(document, "script", "facebook-jssdk");

			// Update config.
			_.assign(this.config, cfg);
			this.injected = true;
		}
		else
			this.readyRejector();

		return this.ready;
	}

	openMessenger()
	{
		return this.allowInjectionForThisBrowser()
			? this.ready.then(() => FB.CustomerChat.showDialog())
			: Promise.resolve();
	}

	event()
	{
		return this.allowInjectionForThisBrowser()
			? this.ready.then(() => FB.Event)
			: Promise.resolve(null);
	}

	showPlugin()
	{
		return this.allowInjectionForThisBrowser()
			? this.ready.then(() => FB.CustomerChat.show(false))
			: Promise.resolve();
	}

	hidePlugin()
	{
		return this.allowInjectionForThisBrowser()
			? this.ready.then(() => FB.CustomerChat.hide())
			: Promise.resolve();
	}
}
