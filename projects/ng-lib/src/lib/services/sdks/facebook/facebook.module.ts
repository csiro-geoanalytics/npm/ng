import { NgModule, ModuleWithProviders, Optional, SkipSelf } from "@angular/core";
import { FacebookService, FacebookServiceConfig } from "./facebook.service";


@NgModule()
export class FacebookModule
{
	constructor(@Optional() @SkipSelf() parentModule: FacebookModule)
	{
		if (parentModule)
			throw new Error("FacebookModule is already loaded.");
	}

	static forRoot(config?: FacebookServiceConfig): ModuleWithProviders<FacebookModule>
	{
		return {
			ngModule: FacebookModule,
			providers: [
				{ provide: FacebookServiceConfig, useValue: config },
				{ provide: FacebookService }
			]
		};
	}
}
