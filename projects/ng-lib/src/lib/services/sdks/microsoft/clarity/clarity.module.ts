import { NgModule, ModuleWithProviders, Optional, SkipSelf } from "@angular/core";
import { ClarityService, ClarityServiceConfig } from "./clarity.service";


@NgModule()
export class ClarityModule
{
	constructor(@Optional() @SkipSelf() parentModule: ClarityModule)
	{
		if (parentModule)
			throw new Error("ClarityModule is already loaded.");
	}

	static forRoot(config?: ClarityServiceConfig): ModuleWithProviders<ClarityModule>
	{
		return {
			ngModule: ClarityModule,
			providers: [
				{ provide: ClarityServiceConfig, useValue: config },
				{ provide: ClarityService }
			]
		};
	}
}
