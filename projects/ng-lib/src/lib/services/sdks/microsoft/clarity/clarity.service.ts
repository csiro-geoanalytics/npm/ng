import { Injectable } from "@angular/core";
import { GenericSdk, GenericSdkConfig } from "../../generic-sdk.service";

import * as _ from "lodash";


export class ClarityServiceConfig extends GenericSdkConfig {}

@Injectable()
export class ClarityService extends GenericSdk
{
	constructor(
		readonly config				: ClarityServiceConfig
	)
	{
		super(config);
	}

	inject(config?: ClarityServiceConfig)
	{
		if (this.injected)
			return this.ready;

		const cfg = { ...this.config, ...config };

		this.assertApiKey(cfg);
		if (this.allowInjectionForThisBrowser(cfg))
		{
			((c, l, a, r, i, t, y) => {
				// eslint-disable-next-line prefer-arrow/prefer-arrow-functions
				c[a] = c[a] || function() { (c[a].q = c[a].q || []).push(arguments); };
				t = l.createElement(r);
				t.async = true;
				t.defer = true;
				t.onload = this.readyResolver;
				t.onerror = this.readyRejector;
				t.src = "https://www.clarity.ms/tag/" + i;
				y = l.getElementsByTagName(r)[0];
				y.parentNode.insertBefore(t, y);
			})(window, document, "clarity", "script", cfg.key);

			// Update config.
			_.assign(this.config, cfg);
			this.injected = true;
		}
		else
			this.readyRejector();

		return this.ready;
	}
}
