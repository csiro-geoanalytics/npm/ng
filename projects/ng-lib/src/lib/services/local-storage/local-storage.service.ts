import { Injectable, Inject, InjectionToken, Optional, OnInit, inject, PLATFORM_ID } from "@angular/core";
import { isPlatformBrowser } from "@angular/common";

import { default as ssw } from "localforage-sessionstoragewrapper/dist/localforage-sessionstoragewrapper.es6";

import * as _		from "lodash";
import * as lf 		from "localforage";


export const LOCALSTORAGE_CONFIG = new InjectionToken("local-storage.service.localstorageconfig");

@Injectable()
export class LocalStorageService implements OnInit
{
	private platformId = inject(PLATFORM_ID);

	readonly SESSIONSTORAGE = ssw._driver;

	constructor(
		@Optional()
		@Inject(LOCALSTORAGE_CONFIG)
		public config: LocalForageDbInstanceOptions
	)
	{
		// Define SessionStorage driver just once to avoid redefinition warning that appears in Angular 17 hydration set up.
		if (isPlatformBrowser(this.platformId) && !lf.supports(this.SESSIONSTORAGE))
			lf.defineDriver(ssw);
	}

	ngOnInit()
	{
		if (this.config)
			lf.config(this.config);
	}

	getInstance(storeName?: string, options?: LocalForageOptions): LocalForage
	{
		return lf.createInstance(
			_.assign(options || {}, this.config, storeName ? { storeName } : {})
		);
	}

	dropInstance(storeName: string): Promise<void>
	{
		return lf.dropInstance(_.defaults({ storeName }, this.config));
	}

	dropInstances(storeNames: string[]): Promise<void>
	{
		// Important: drop stores sequentially to avoid synchronisation issues when database version is being upgraded.
		return storeNames.reduce(async (prev, next) => {
			await prev;
			return this.dropInstance(next);
		}, Promise.resolve());
	}

	queryUsageAndQuota(): Promise<StorageEstimate>
	{
		return navigator.storage.estimate();
	}

	getObjectStoreNames(): Promise<string[]>
	{
		let resolver	: (value?: string[] | PromiseLike<string[]>) => void;
		const promise	= new Promise<string[]>(resolve => resolver = resolve);

		(<any>indexedDB).open(this.config?.name ?? "localforage")
			.onsuccess = (event: any) => resolver(_.map(event.target.result.objectStoreNames, v => v));
		return promise;
	}

	async getNonSystemStoreNames(): Promise<string[]>
	{
		return this.getObjectStoreNames()
			.then(names => _.filter(names, name => !name.startsWith("//") && name !== "local-forage-detect-blob-support"));
	}
}
