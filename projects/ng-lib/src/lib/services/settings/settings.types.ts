import { InjectionToken } from "@angular/core";
import { Observable } from "rxjs";

export const SETTINGS_CONFIG = new InjectionToken<SettingsConfig>("app-settings.service.config");
export const SETTINGS_ENUM = new InjectionToken<SettingsEnum>("app-settings.service.settings-enum");

export const enum Store
{
	// session and local storage must be listed in the order of preference, i.e. when a value from 'both' is requested
	// then session storage is tried first and local storage is used as a fallback.
	session		= 0x1,
	local		= 0x2,
	// eslint-disable-next-line no-bitwise
	both		= local | session
}

export interface Property
{
	store		: Store;
	key?		: string;
}

export interface UpdatedSettingEvent
{
	store		: Store;
	key			: string;
	value		: any;
}

export type SettingsEnum = Record<string, Property>;

export interface SettingsConfig
{
	// Default store name.
	storeName		: string;

	// Default expiry for expirable settings.
	expiry			: number;

	// Delay before changes to application settings are synchronised with the Cloud.
	syncDelay		: number;

	// Validity period before settings will be restored from the Cloud.
	syncValidity	: number;
}

export interface CloudSync
{
	cloudSyncUpload(data: any): Observable<void>;
	cloudSyncDownload(): Observable<any>;
}
