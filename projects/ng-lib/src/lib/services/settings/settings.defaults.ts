import { SettingsConfig } from "./settings.types";

export const CONFIG: SettingsConfig = {

	storeName		: "//settings",

	expiry			: 2592000,	// 30 days

	syncDelay		: 30000,	// 30 seconds
	syncValidity	: 3600000	// 1 hour

};
