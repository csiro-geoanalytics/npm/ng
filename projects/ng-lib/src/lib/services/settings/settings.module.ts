import { NgModule, ModuleWithProviders } from "@angular/core";
import { SettingsService } from "./settings.service";

import * as _		from "lodash";
import * as TT		from "./settings.types";
import * as DEF		from "./settings.defaults";


@NgModule()
export class SettingsModule
{
	static forRoot(settings?: TT.SettingsEnum, config?: Partial<TT.SettingsConfig>): ModuleWithProviders<SettingsModule>
	{
		// App settings.
		this.initSettingsEnum(settings);

		return {
			ngModule: SettingsModule,
			providers: [
				SettingsService,
				{ provide: TT.SETTINGS_CONFIG, useValue: { ...DEF.CONFIG, ...config } },
				{ provide: TT.SETTINGS_ENUM, multi: true, useValue: settings }
			]
		};
	}

	static forChild(settings?: TT.SettingsEnum): ModuleWithProviders<SettingsModule>
	{
		this.initSettingsEnum(settings);
		return {
			ngModule: SettingsModule,
			providers: [
				{ provide: TT.SETTINGS_ENUM, multi: true, useValue: settings }
			]
		};
	}

	private static initSettingsEnum(settings: TT.SettingsEnum)
	{
		// Automatically assign SettingsEnum keys.
		_.each(_.keys(settings), k => settings[k].key = k);
	}
}
