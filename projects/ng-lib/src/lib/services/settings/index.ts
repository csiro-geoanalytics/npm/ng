// start:ng42.barrel
export * from "./settings.defaults";
export * from "./settings.module";
export * from "./settings.service";
export * from "./settings.types";
// end:ng42.barrel

