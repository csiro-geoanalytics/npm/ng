import { Inject, Injectable, Optional } from "@angular/core";
import { Router } from "@angular/router";
import { SubscriptionManager } from "@csiro-geoanalytics/utils-rxjs";
import { LocalStorageService } from "../local-storage";
import { LocalSorageCache, LocalStorageCacheOptions } from "../cache";
import { Readiable } from "../../utils/readiable";

import { Subject, from, merge, of } from "rxjs";
import { switchMap, auditTime, takeUntil, tap, map, mergeAll, skipWhile, first, filter } from "rxjs/operators";


import * as TT		from "./settings.types";
import * as _		from "lodash";

import "localforage";


// Service internal settings.
const settingsEnum /*: SettingsEnum */ = {

	appSettingsUserID				: { store: TT.Store.local },
	appSettingsCloudSync			: { store: TT.Store.local },
	appSettingsCloudSyncExpiry		: { store: TT.Store.session },

};

@Injectable()
export class SettingsService extends Readiable
{
	private stores					: [ LocalForage, LocalForage ];
	private subscriptions			= new SubscriptionManager();
	private expirableSettings		: LocalSorageCache;
	private _cloudSyncEnabled?		: boolean;
	private _cloudSyncProvider?		: TT.CloudSync;

	// Events.
	private  updated				= new Subject<TT.UpdatedSettingEvent>();
	private  reload					= new Subject<boolean>();
	private  sync					= new Subject<void>();
	private  userChanged			= new Subject<string>();
	readonly updated$				= this.updated.asObservable();
	readonly reload$				= this.reload.asObservable();
	readonly userChanged$			= this.userChanged.asObservable();

	constructor(
		@Inject(TT.SETTINGS_CONFIG)
		private config				: TT.SettingsConfig,

		@Optional()
		private router				: Router,

		private localStorage		: LocalStorageService
	)
	{
		super();

		// Automatically assign SettingsEnum keys.
		_.each(_.keys(settingsEnum), k => (<any>settingsEnum)[k].key = k);

		// Register persistent and session stores.
		this.stores = [
			this.localStorage.getInstance(this.config.storeName, { driver: this.localStorage.SESSIONSTORAGE }),
			this.localStorage.getInstance(this.config.storeName)
		];

		// Instantiate LocalSorageCache for expirable settings.
		this.expirableSettings = new LocalSorageCache(this.stores[1], { expiry: () => this.config.expiry });

		// Subscribe to updates.
		this.subscriptions.register(
			merge(
				this.reload$.pipe(
					// Skip reload$ after CloudSync restore.
					filter(syncToCloud => syncToCloud)
				),
				this.updated$.pipe(
					// Throttle events.
					auditTime(this.config.syncDelay)
				)
			)
			.pipe(
				// No sync needed.
				skipWhile(() => !this._cloudSyncEnabled),
				// Skip pending updated$ events.
				takeUntil(this.sync)
			),
			{
				// Periodically backup settings into the Cloud.
				next: () => this.cloudSyncBackup(),

				// Keep subscriptions alive after error and this.sync event.
				error	: () => this.subscriptions.subscribeAll(),
				complete: () => this.subscriptions.subscribeAll()
			}
		);

		// Start monitoring changes when ready.
		this.ready.then(() => this.subscriptions.subscribeAll());
	}

	async get<T>(property: TT.Property, defaultValue?: T): Promise<T>;
	async get<T>(store: TT.Store, key: string, defaultValue?: T): Promise<T>;
	async get<T>(...params: any)
	{
		const [ store, key, defaultValue ] = this.argumentsStoreKeyValueOverloading<T>(...params);
		return Promise
			.all(
				_.map(this.getStore(store), v => v.getItem<T>(key))
			)
			.then(res => {
				let obj: T = null;
				_.each(res, v => {
					if (v != null)
					{
						obj = v;
						return false;
					}
					return true;
				});
				return obj == null ? defaultValue : obj;
			});
	}

	async set<T>(property: TT.Property, value: T): Promise<T>;
	async set<T>(store: TT.Store, key: string, value: T): Promise<T>;
	async set<T>(...params: any)
	{
		const [ store, key, value ] = this.argumentsStoreKeyValueOverloading<T>(...params);
		if (store !== TT.Store.session)
			this.updated.next({ store, key, value });
		if (value === null || value === undefined)
			return this.remove(store, key).then(() => value);
		else
			return Promise.all(_.map(this.getStore(store), v => v.setItem<T>(key, value))).then(() => value);
	}

	async remove(property: TT.Property): Promise<void>;
	async remove(store: TT.Store, key: string): Promise<void>;
	async remove(...params: any): Promise<void>
	{
		const [ store, key ] = this.argumentsStoreKeyOverloading(...params);
		if (store !== TT.Store.session)
			this.updated.next({ store, key, value: null });
		return Promise.all(_.map(this.getStore(store), v => v.removeItem(key))).then(() => undefined);
	}

	async clear(): Promise<void>
	{
		this._cloudSyncEnabled = true;
		return Promise.all(_.map(this.stores, store => store.clear()))
			.then(() => this.reload.next(true));
	}

	private getStore(store: TT.Store): LocalForage[]
	{
		return store === TT.Store.both ? this.stores : [ this.stores[store - 1] ];
	}

	private argumentsStoreKeyOverloading(...params: any): [ number, string ]
	{
		return _.isNumber(params[0])
			? <[ number, string ]>[ ...params ]
			: <[ number, string ]>[ (<TT.Property>params[0]).store, (<TT.Property>params[0]).key ];
	}

	private argumentsStoreKeyValueOverloading<T>(...params: any): [ number, string, T ]
	{
		return _.isNumber(params[0])
			? <[ number, string, T ]>[ ...params ]
			: <[ number, string, T ]>[ (<TT.Property>params[0]).store, (<TT.Property>params[0]).key, params[1] ];
	}

	/*
	 #######
	 #       #    # #####  # #####    ##   #####  #      ######     ####  ###### ##### ##### # #    #  ####
	 #        #  #  #    # # #    #  #  #  #    # #      #         #      #        #     #   # ##   # #    #
	 #####     ##   #    # # #    # #    # #####  #      #####      ####  #####    #     #   # # #  # #
	 #         ##   #####  # #####  ###### #    # #      #              # #        #     #   # #  # # #  ###
	 #        #  #  #      # #   #  #    # #    # #      #         #    # #        #     #   # #   ## #    #
	 ####### #    # #      # #    # #    # #####  ###### ######     ####  ######   #     #   # #    #  ####
	*/

	async getExpirable<T>(key: string, defaultValue?: T): Promise<T | unknown>
	{
		return this.expirableSettings
			.observable(
				key,
				of(undefined),
				// Never cache anything on GET request.
				{ cacheCondition: () => false })
			.toPromise()
			.then(val => val ?? defaultValue);
	}

	async setExpirable<T>(key: string, value: T, options?: Partial<LocalStorageCacheOptions<T>>): Promise<T | unknown>
	{
		return this.remove(TT.Store.local, key)
			.then(() => this.expirableSettings.observable(key, of(value), options).toPromise());
	}

	/*
	 ######
	 #     #  ####  #    # ##### ######         ##   #    #   ##   #####  ######
	 #     # #    # #    #   #   #             #  #  #    #  #  #  #    # #
	 ######  #    # #    #   #   #####  ##### #    # #    # #    # #    # #####
	 #   #   #    # #    #   #   #            ###### # ## # ###### #####  #
	 #    #  #    # #    #   #   #            #    # ##  ## #    # #   #  #
	 #     #  ####   ####    #   ######       #    # #    # #    # #    # ######

	Route-aware settings.
	*/

	async getRouteAware<T>(property: TT.Property, defaultValue?: T): Promise<T>
	{
		return this.get(property, {}).then(config => config[this.getFormattedUrl()] ?? defaultValue);
	}

	async setRouteAware<T>(property: TT.Property, value: T): Promise<T>
	{
		return this.get(property, {})
			.then(async config => {
				config[this.getFormattedUrl()] = value;
				return this.set(property, config).then(() => value);
			});
	}

	private getFormattedUrl()
	{
		if (!this.router)
			throw new Error("Router is not found in the Injector.");
		return this.router.url.substring(1).replace(/[^\w]/gi, "-");
	}

	/*
	 ######  ##        #######  ##     ## ########      ######  ##    ## ##    ##  ######
	##    ## ##       ##     ## ##     ## ##     ##    ##    ##  ##  ##  ###   ## ##    ##
	##       ##       ##     ## ##     ## ##     ##    ##         ####   ####  ## ##
	##       ##       ##     ## ##     ## ##     ##     ######     ##    ## ## ## ##
	##       ##       ##     ## ##     ## ##     ##          ##    ##    ##  #### ##
	##    ## ##       ##     ## ##     ## ##     ##    ##    ##    ##    ##   ### ##    ##
	 ######  ########  #######   #######  ########      ######     ##    ##    ##  ######
	*/

	setCloudSyncProvider(provider: TT.CloudSync)
	{
		this._cloudSyncProvider = provider;
	}

	get cloudSyncProvider()
	{
		return this._cloudSyncProvider;
	}

	get cloudSyncEnabled()
	{
		return this._cloudSyncEnabled;
	}

	async setCloudSyncEnabled(value: boolean, localOnly = false)
	{
		return this.set(settingsEnum.appSettingsCloudSync, this._cloudSyncEnabled = value)
			.then(() => localOnly ? null : this.cloudSyncBackup())
			.then(() => value)
			.catch(() => false)
		;
	}

	init(userId?: string): Promise<void>
	{
		return (
				userId
					? this.get(settingsEnum.appSettingsUserID)
						// Reset settings when users change.
						.then(last => {
							if (userId === last)
								return Promise.resolve();
							else
							{
								this.userChanged.next(userId);
								return this.clearSilent();
							}
						})
						// Check last sync time.
						.then(() => this.get<Date>(settingsEnum.appSettingsCloudSyncExpiry))
						// Restore user's settings from the Cloud.
						.then(expiry =>
							expiry && (new Date(expiry)).getTime() > Date.now()
								? this.get(settingsEnum.appSettingsCloudSync, false)
									.then(sync => this._cloudSyncEnabled = sync)
								: this.cloudSyncRestore()
						)
					: this.setCloudSyncEnabled(undefined, true)
			)
			// Set local User ID.
			.then(() => <Promise<void>>(userId ? this.set(settingsEnum.appSettingsUserID, userId) : Promise.resolve()))
			.catch(error => {
				console.error(error);
				this.setCloudSyncEnabled(undefined, true);
				throw error;
			})
			.finally(this.readyResolver)
		;
	}

	/**
	 * Doesn't emit events or triggers CloudSync.
	 */
	private async clearSilent(): Promise<void>
	{
		return Promise.all(_.map(this.stores, store => store.clear())).then();
	}

	cloudSyncBackup(): Promise<void>
	{
		if (!this._cloudSyncProvider)
			return Promise.reject("CloudSyncProvider is not set.");
		return (this._cloudSyncEnabled ? from(this.serializeLocal()) : of({}))
			.pipe(
				tap(() => this.sync.next()), // Skip any pending app settings update$ events.
				switchMap(data => {
					data.appSettingsCloudSync = this._cloudSyncEnabled;
					return this._cloudSyncProvider.cloudSyncUpload(data);
				}),
				// Update expiry date.
				tap(() => this.set(settingsEnum.appSettingsCloudSyncExpiry, new Date(Date.now() + this.config.syncValidity))),
				// Ensures the resulting Promise always resolves.
				first()
			)
			.toPromise();
	}

	private cloudSyncRestore(): Promise<any>
	{
		// If provider is not set, consider it disabled.
		if (!this._cloudSyncProvider)
			return Promise.resolve({ appSettingsCloudSync: this._cloudSyncEnabled = false });

		return this._cloudSyncProvider.cloudSyncDownload()
			.pipe(
				first(), // Ensures the resulting Promise always resolves.
				map(data => {
					data = data || {};
					this._cloudSyncEnabled = data.appSettingsCloudSync = (data.appSettingsCloudSync !== false);
					return this._cloudSyncEnabled ? data : { appSettingsCloudSync: false };
				}),
				// Restore setting to LocalStorage.
				switchMap(data => this.deserializeLocal(data)),
				// Update expiry date.
				tap(() => this.set(settingsEnum.appSettingsCloudSyncExpiry, new Date(Date.now() + this.config.syncValidity))),
				// Broadcast application settings update.
				tap(() => this.reload.next(false))
			)
			.toPromise();
	}

	private async deserializeLocal(data: any)
	{
		if (!data)
			return Promise.resolve(data);
		const store = this.getStore(TT.Store.local)[0];
		return from(_.keys(data))
			.pipe(
				map(key => store.setItem(key, data[key])),
				mergeAll(),
				map(() => data)
			)
			.toPromise();
	}

	private async serializeLocal(): Promise<{ [ name: string ]: any }>
	{
		const
			store = this.getStore(TT.Store.local)[0],
			ret = {}
		;
		return store.iterate((v, key) => {
				if (v !== null && v !== undefined)
					ret[key] = v;
			})
			.then(() => ret);
	}
}
