import { Injectable, InjectionToken, Inject } from "@angular/core";
import { HttpClient } from "@angular/common/http";

import { from, of } from "rxjs";
import { catchError, shareReplay } from "rxjs/operators";


export const CONTEXT_CONFIG = new InjectionToken<ContextConfig>("context.service.config");

@Injectable()
export class ContextService
{
	readonly ciDeployInfo$			= this.config.enableDeployConfig ? from(this.http.get("ci-deploy-info.json")).pipe(catchError(() => of({})), shareReplay(1)) : of({});

	constructor(
		@Inject(CONTEXT_CONFIG)
		readonly config				: ContextConfig,
		private  http				: HttpClient
	) {}

	static async load(configUrl: string = "./contexts/context.json"): Promise<ContextConfig>
	{
		const response = await fetch(configUrl);
		return await response.json();
	}
}

export interface ContextConfig
{
	enableDeployConfig?: boolean;
	[ prop: string ]: any;
}
