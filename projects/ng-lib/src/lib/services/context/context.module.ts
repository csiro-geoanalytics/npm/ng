import { NgModule } from "@angular/core";
import { ContextService } from "./context.service";


@NgModule({
	providers: [ ContextService ]
})
export class ContextModule {}
