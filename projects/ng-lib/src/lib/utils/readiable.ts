export abstract class Readiable<T = unknown>
{
	readonly ready					= new Promise<T>((resolve, reject) => { this.readyResolver = resolve; this.readyRejector = reject; });
	protected readyResolver			: (value?: T | PromiseLike<T>) => void;
	protected readyRejector			: (reason?: any) => void;
}
